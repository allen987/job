package cn.jiangzeyin.system;

import cn.jiangzeyin.entity.IQuartzInfo;
import com.alibaba.fastjson.JSONArray;

import java.util.List;

/**
 * 动态调度数据接口
 * Created by jiangzeyin on 2017/8/22.
 *
 * @author jiangzeyin
 */
public interface JobDataInterface {
    /**
     * 获取所有调度信息，包括已经暂停的和删除的
     *
     * @param debug 调试信息
     * @return 数据
     */
    List<IQuartzInfo> getAll(String debug);

    /**
     * 修改调度运行数据
     *
     * @param entityClass 实体信息
     * @param jobId       调度id
     * @param data        运行数据
     */
    void update(Class<?> entityClass, int jobId, String data);

    /**
     * 获取调度运行数据
     *
     * @param entityClass 实体
     * @param jobId       调度id
     * @return 所有运行数据
     */
    JSONArray getJobRunData(Class<?> entityClass, int jobId);

    /**
     * 获取默认的调度实体信息
     *
     * @return class
     */
    Class<?> getJobDefaultClass();
}
