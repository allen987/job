package cn.jiangzeyin.system;

/**
 * Created by jiangzeyin on 2017/8/22.
 *
 * @author jiangzeyin
 */
public class JobPropertiesInfo {
    /**
     * 监听事件 cron
     */
    public static final String SYSTEM_DETECTION_CRON = "system_detection_cron";
    /**
     * 默认表达式
     */
    public static final String SYSTEM_DETECTION_CRON_DEFAULT_VALUE = "0 0/1 * * * ?";

    public static final String TAG = "tag";

    public static final String JOB_RUN_STATUS_ENTITY_TIME_FORM_MART = "run_status_time";
    /**
     * 时间格式表达式
     */
    public static final String JOB_RUN_STATUS_ENTITY_TIME_FORM_MART_DEFAULT_VALUE = "yyyy-MM-dd HH:mm:ss SSS";

    public static final String RUN_DATA_KEY_NAME = "run_data_key_name";

    public static final String RUN_DATA_VALUE_NAME = "run_data_value_name";
    /**
     * 调试运行
     */
    public static final String DEBUG = "debug";
    /**
     * 单机运行模式
     */
    public static final String STAND_ALONE = "standAlone";
}
