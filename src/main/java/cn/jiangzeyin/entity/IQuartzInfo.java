package cn.jiangzeyin.entity;

import cn.jiangzeyin.system.SystemJobManager;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * 调度信息接口
 * Created by jiangzeyin on 2017/8/22.
 *
 * @author jiangzeyin
 */
public interface IQuartzInfo {
    /**
     * 数据id
     *
     * @return 数据id
     */
    int getId();

    /**
     * 调用名称
     *
     * @return 调用名称
     */
    String getName();

    /**
     * 分组名
     *
     * @return 分组名
     */
    String getGroupName();

    /**
     * 运行class
     *
     * @return class
     */
    String getRunClass();

    /**
     * 运行cron 表达式
     *
     * @return string
     */
    String getCron();

    /**
     * 当前调度运行状态
     *
     * @return 枚举值
     * @see Status
     */
    int getStatus();

    /**
     * 当前调度是否被删除
     *
     * @return 删除 true
     */
    boolean isDelete();

    /**
     * 当前调度是否是活跃状态
     *
     * @return 活跃 true
     */
    boolean isActive();

    /**
     * 请重写改方法，方便日志显示信息
     *
     * @return 实体信息
     */
    @Override
    String toString();

    /**
     * 运行状态
     */
    enum Status {
        /**
         *
         */
        Run(0, "正在运行"),
        Stop(1, "未运行"),
        Error(2, "运行异常");

        private int code;
        private String desc;

        Status(int code, String desc) {
            this.code = code;
            this.desc = desc;
        }

        public int getCode() {
            return code;
        }

        public String getDesc() {
            return desc;
        }
    }

    /**
     * 转换
     *
     * @param jsonArray array
     * @return obj
     */
    static JSONObject convertJSONObject(JSONArray jsonArray) {
        if (jsonArray == null) {
            return null;
        }
        JSONObject jsonObject = new JSONObject();
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject object = jsonArray.getJSONObject(i);
            jsonObject.put(object.getString(SystemJobManager.RunData.getKeyName()), object.getString(SystemJobManager.RunData.getValueName()));
        }
        return jsonObject;
    }

}
